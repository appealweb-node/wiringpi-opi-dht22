FROM arm64v8/node:lts-alpine3.12

RUN apk update && apk add --no-cache \
        gcc \
        binutils \
        build-base \
        libgcc \
        make \
        musl-dev \
        g++ \
        libc-dev \
        linux-headers \
        nano \
        git

WORKDIR /opt/gpio

RUN git clone https://gitlab.com/appealweb-libraries/wiringop-zero-alpinelinux.git

WORKDIR /opt/gpio/wiringop-zero-alpinelinux

RUN ./build

WORKDIR /appealweb/test-dht22

RUN echo $' \n  var d = require(\'wiringpi-opi-dht22\'); \n var t = d.reader({pin: 8, delay: 1000}); \n t.on(\'data\', function (data) { \n console.log(\'farenheit: \'+data.farenheit+\' celsius: \'+data.celsius+\' humidity: \'+data.humidity); \n });' > index.js

RUN npm install wiringpi-opi-dht22

ENTRYPOINT ["sh"]
