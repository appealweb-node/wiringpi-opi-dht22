var evt = require('events').EventEmitter,
    spawn = require('child_process').spawn,
    path = require('path'),
    util = require('util');

dht22.SCRIPT = (process.platform === 'win32')?'build/Release/dht22.exe':'build/Release/dht22';
dht22.process = null;
dht22.instance = null;

function dht22(options){

    evt.call(this);

    dht22.process = spawn(
        // create command path
        path.join(path.normalize(path.join(__dirname, '..')), dht22.SCRIPT)
        // set the command parameters
        , [options.pin, options.delay]);

    dht22.process.stdout.on(
        'data',
        this.onData.bind(this)
    );

    dht22.process.stderr.on(
        'data',
        this.onError.bind(this)
    );

}

util.inherits(dht22, evt);

dht22.getInstance = function(options){
    return dht22.instance || (dht22.instance = new dht22(options));
};

dht22.prototype.onData = function(buffer){
    this.emit('data', JSON.parse(buffer.toString()));
};

dht22.prototype.onError = function(error){
    this.emit('error', error);
};

dht22.prototype.kill = function(){
    if(!!dht22.process) {
        dht22.process.kill();
    }
};

//Kill dht22 process when exit
_this = dht22;
process.on('SIGINT', function() {
    _this.process.kill();
    process.exit();
    });

module.exports = dht22;