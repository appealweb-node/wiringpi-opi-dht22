var dth = require('../index');
var assert = require('chai').assert;

var d = dth.reader({pin: 7, delay: 10});

describe('Test dht22 object',function(){
    it('must create the object with pre-defined parameters.', function(){
        assert.isNotNull(d);
    });

    it('check if is type of object.', function(){
        assert.typeOf(d, 'object');
    });

    it('check if is object of type DHT 22.', function(){
        assert.equal(d.constructor.name, 'dht22');
    });
});

describe('Receive data from child_process', function(){
    var readedObj = false;
    it('returned is a correct object.', function(done){
        d.on('data', function(data){
            if(!readedObj){
                readedObj = true;
                
                assert.isObject(data);
                done();
            }
        });
    });

    var readedTemp = false;
    it('read temperature.', function(done){
        d.on('data', function(data){
            if(!readedTemp){
                readedTemp = true;
                
                assert.isNumber(data.celsius);
                done();
            }
        });
    });
    
    var readedHumidity = false;
    it('read humidity.', function(done){
        d.on('data', function(data){
            if(!readedHumidity){
                readedHumidity = true;
                
                assert.isNumber(data.humidity);
                d.kill();
            }
            done();
        });
    });
});

