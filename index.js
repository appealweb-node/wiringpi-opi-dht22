var dht22 = require('./lib/dht22');

module.exports = {

    reader: function(options){

        if(typeof(options) === typeof(undefined)){
            options = {pin: 8, delay: 1000};
        }

        return dht22.getInstance(options);
    }
};