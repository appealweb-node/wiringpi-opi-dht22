wiringpi-opi-dht22
---
## Dependencies
[*wiringop-zero-alpinelinux*](https://gitlab.com/appealweb-libraries/wiringop-zero-alpinelinux)

### Description
NodeJS module to read data from a DHT 22/DHT 11 temperature and humidity sensor on boards that supports WiringPi library to access GPIO ports. Can be used in Alpine Linux versions.

Tested on OrangePi Zero Plus H2 / H5

[Node package](https://www.npmjs.com/package/wiringpi-opi-dht22)

## Installation
#### 1) build & install WiringOP or WiringPi
```bash
pi@orangepi ~ $ git clone https://gitlab.com/appealweb-libraries/wiringop-zero-alpinelinux
...
pi@orangepi ~ $ cd wiringop-zero-alpinelinux
pi@orangepi ~/wiringop-zero-alpinelinux $ sudo su
...
pi@orangepi ~/wiringop-zero-alpinelinux $ ./build
```
#### 2) install node module
```bash
npm install wiringpi-opi-dht22
```

#### 3) use node module

```javascript
var d = require('wiringpi-opi-dht22');

var t = d.reader({pin: 8, delay: 1000});

t.on('data', function (data) {
  console.log(+' farenheit: '+data.farenheit + ' celsius: '+data.celsius+' humidity: '+data.humidity);
});
```
## Dockerfile (Docker container)
You can build the image with the [Dockerfile file](Dockerfile)
```
    docker build -t <image-name:tag-number> .

    i.e.: docker build -t test-dht22:1.0 .
```

and then run the created image with privileged permissions.
```
    docker run --privileged --device=/dev/mem:/dev/mem -it --rm <image-name:tag-number>

    i.e.: docker run --privileged --device=/dev/mem:/dev/mem -it --rm test-dht22:1.0
```
once inside the image run the test command: 
```bash 
node index.js 
```